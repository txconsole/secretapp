package com.txconsole.security;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.security.KeyStore;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.springframework.context.annotation.Configuration;

@Configuration
public class Mykeytool {
	
	/*
	 * password to be stored => Eureka@123
	 * keystore password     => storepass123
	 *  
	 * Keytool to create keystore :
	 *      keytool -importpass -storetype jceks -alias PasswordEntry -keystore password-store.jceks
	 *      
	 */		    
	
	 public String decodeSecret(String mountPath) throws Exception {
		 String storePassword = "storepass123";
		// String mountPath = "/home/rajesing/eclipse-workspace/KeyUtils/password-store.jceks";
		      //String mountPath = "/tmp/apikeyPath/userpassword";
		      //String storePassword = "storepass123";
		 //String passwordPassword = "pass123";
		 String passwordAlias = "PasswordEntry";

	        KeyStore keyStore = loadKeyStoreFromFile(mountPath, storePassword);
	        String password = readPasswordFromKeyStore(keyStore, storePassword, passwordAlias);
	        System.out.println("read password " + password );
	        return password;
	    }
	
	
	private static String readPasswordFromKeyStore(KeyStore keyStore, String passwordPassword, String passwordAlias) throws Exception {

        KeyStore.PasswordProtection keyStorePP = new KeyStore.PasswordProtection(passwordPassword.toCharArray());
        KeyStore.SecretKeyEntry ske =
                (KeyStore.SecretKeyEntry)keyStore.getEntry(passwordAlias, keyStorePP);

        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBE");
        PBEKeySpec keySpec = (PBEKeySpec)factory.getKeySpec(
                ske.getSecretKey(),
                PBEKeySpec.class);

        return new String(keySpec.getPassword());
    }
	
	public static KeyStore loadKeyStoreFromFile(String pathToFile, String keystorePassword)
            throws Exception {
        KeyStore keyStore = KeyStore.getInstance("JCEKS");
        keyStore.load(getFileInputStreamFromArg(pathToFile), keystorePassword.toCharArray());
        return keyStore;
    }
	
	public static FileInputStream getFileInputStreamFromArg(String filePath) throws FileNotFoundException {
        File file = new File(filePath);
        return new FileInputStream(file);
    }	

}
