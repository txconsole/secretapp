package com.txconsole.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Value("${userpassword}")
	String password;
	
	@Value("${PassMountPath}")
	String mountPath;

    
	
	@Autowired
	Mykeytool mykeytool;
	
    @Override
    protected void configure(HttpSecurity http) throws Exception 
    {
        http
         .csrf().disable()
         .authorizeRequests().anyRequest().authenticated()
         .and()
         .httpBasic();
    }
 
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) 
            throws Exception 
    {
    	System.out.println("User Password passed is "+ password);
       // auth.inMemoryAuthentication().withUser("admin").password(mykeytool.decodeSecret(mountPath)).roles("USER");
        auth.inMemoryAuthentication().withUser("admin").password(password).roles("USER");
    }
}