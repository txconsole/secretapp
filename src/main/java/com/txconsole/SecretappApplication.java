package com.txconsole;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecretappApplication {

	public static void main(String[] args) {
		SpringApplication.run(SecretappApplication.class, args);
	}

}
